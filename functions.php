<?php add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style', get_stylesheet_uri(), array('parent-style'));
}

function load_comit_scripts()
{

    wp_register_script('click_track', ("/wp-content/custom/js/click-track.js"), array('jquery'), false, false);
    wp_enqueue_script('click_track');
}

add_action('wp_enqueue_scripts', 'load_comit_scripts');

function wpbsearchform($form)
{

    $form = '<form role="search" method="get" id="searchform" action="' . home_url('/') . '" >
    <div><label style="display: none;" class="screen-reader-text" for="s">' . __('Search for:') . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="' . esc_attr__('&#x55;') . '" />
    </div>
    </form>';

    return $form;
}

add_shortcode('wpbsearch', 'wpbsearchform');

add_action('init', 'create_post_type');
function create_post_type()
{
    register_post_type('product',
        array(
            'labels' => array(
                'name' => __('Products'),
                'singular_name' => __('Product')
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}